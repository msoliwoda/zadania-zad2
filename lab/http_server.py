# -*- encoding: utf-8 -*-

import socket
import datetime
import email.utils
from time import strftime, gmtime
def http_serve(server_socket, html):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()

        try:
            # Odebranie żądania
            request = connection.recv(1024)
            if request:
                print("Odebrano:")
                print(request)
                request_method = str(request).split(' ')[0]
                print(request_method)
                request_method1 = str(request).split(' ')[2]
                print(request_method1)

                uri = str(request).split(' ')[1]
                print(uri)
                header=  'HTTP/1.1 200 OK\n'

                header += 'GMT date: ' + str(strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime())) +'\n'
                print(header)
                response_content = b"\r\n<html><body><p>Error 404: File not found</p><p>Python HTTP server</p></body></html>"
                server_response =  header.encode()+response_content
               # if request_method!="GET":
                #    print('nie jest to get')
                 #   break
                #if not 'HTTP' in request_method1:
                 #   print("nie jest to http")
                  #  break
                # Wysłanie zawartości strony
                #connection.sendall("<html><body><p>Error 404: File not found</p><p>Python HTTP server</p></body></html>")
                connection.send(server_response)

        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('localhost', 4444)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

html = open('web/web_page.html').read()

try:
    http_serve(server_socket, html)

finally:
    server_socket.close()

